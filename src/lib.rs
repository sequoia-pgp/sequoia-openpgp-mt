//! Multi-threaded OpenPGP operations.
//!
//! This crate takes advantage of modern CPU to quickly parse OpenPGP
//! keyrings.

#[macro_use]
mod macros;
pub mod keyring;
